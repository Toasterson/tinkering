package greeterd

import (
	"github.com/gin-gonic/gin"
	"github.com/maxence-charriere/app/pkg/app"
)

type Server struct {
	engine *gin.Engine
}

func New() *Server {
	s := &Server{}
	handler := &app.Handler{
		Name: "Tinkering App",
	}
	s.engine = gin.Default()
	s.engine.GET("/greet", GreeterHandler)
	s.engine.GET("/", gin.WrapH(handler))
	s.engine.NoRoute(gin.WrapH(handler))
	return s
}

func (s *Server) Run(listenAddr string) error {
	return s.engine.Run(listenAddr)
}
