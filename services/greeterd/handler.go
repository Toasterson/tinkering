package greeterd

import (
	"gitlab.com/toasterson/tinkering/golang/pkg/hello"
	"github.com/gin-gonic/gin"
	"net/http"
)

func GreeterHandler(c *gin.Context) {
	name := c.Query("name")
	var msg struct{
		Message string
	}
	msg.Message = hello.Greet(name)
	c.JSON(http.StatusOK, msg)
}
