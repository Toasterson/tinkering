//+build js

package main

import (
	"fmt"
	"syscall/js"

	"github.com/maxence-charriere/app/pkg/app"
)

func main() {
	// Import the components that are used to describe the UI:
	app.Import(
		&Hello{},
	)

	// Defines the component to load when an URL without path is loaded:
	app.DefaultPath = "/hello"

	// Runs the app in the browser:
	app.Run()
}

type Hello struct {
	Name string
}

func (h *Hello) Render() string {
	return `
		<div>
			<h1>
				Hello 
				{{ if .Name}}
					  {{.Name}}!!
				{{ else }}
					  World
				{{ end }}
			</h1>
			<input value="{{.Name}}" oninput="Name" autofocus placeholder="Enter your name to be greeter" onchange="OnChange">
		</div>
	`
}

func (h *Hello) OnChange(e, s js.Value) {
	fmt.Println(e.String())
	fmt.Println(s.String())
}
