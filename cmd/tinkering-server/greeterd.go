package main

import (
	"github.com/spf13/viper"
	"gitlab.com/toasterson/tinkering/golang/services/greeterd"
)

func main() {
	viper.SetDefault("listen", "0.0.0.0:5000")
	viper.AutomaticEnv()
	s := greeterd.New()
	if err := s.Run(viper.GetString("listen")); err != nil {
		panic(err)
	}
}
