package hello

import "fmt"

const DefaultName = "world"

func Greet(name string) string {
	if name == "" {
		name = DefaultName
	}
	return fmt.Sprintf("Hello %s!", name)
}
