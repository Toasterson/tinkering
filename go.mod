module gitlab.com/toasterson/tinkering/golang

go 1.13

require (
	github.com/gin-gonic/gin v1.5.0
	github.com/maxence-charriere/app v0.0.0-20191028004104-dde007e431de
	github.com/spf13/viper v1.6.1
)
