FROM golang:1-alpine AS builder
RUN apk add git
WORKDIR /go/src/tinkering
COPY . .

RUN go get -d -v ./...
RUN GO111MODULE=off go get -u -v github.com/maxence-charriere/app/cmd/goapp
RUN CGO_ENABLED=0 goapp build -v

FROM scratch
ENV GIN_MODE=release
COPY --from=builder /go/src/tinkering/cmd/tinkering-server/tinkering-server /frontend
COPY --from=builder /go/src/tinkering/cmd/tinkering-server/web /web
CMD ["/frontend"]